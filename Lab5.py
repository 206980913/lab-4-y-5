
''''# 1)Haciendo uso de un ciclo anidado FOR simule que: en cada una de las entidades, cada
#persona realiza un pago de cada servicio.

entidad = ["Popular", "Nacional", "BNCR"]
persona = ["Juan", "Alverto", "Yessenia"]
servicio = ["Munisipales", "Luz", "Agua"]

for i in entidad:
    for j in persona:
        for y in servicio:
            print("Se realiso un deposito en el ",i," de: ",j," por consepto de ",y)'''


'''# 2)Haciendo uso de un ciclo WHILE, realice un menú que tenga 3 Opciones principales y
#cada una de estas opciones tenga dos opciones internas. No es necesario realizar ninguna
#acción, únicamente demostrar el comportamiento de los ciclos correctamente aplicados
#en un menú.

def menu1():
    print (30 * "-" , "1-MENU-1" , 30 * "-")
    print ("1. Menu Option Primaria 1")
    print ("2. Menu Option Primaria 2")
    print ("3. Menu Option Primaria 3")
    print ("4. Salir")
    print (67 * "-")

def menu2():
    print (30 * "-" , "2-MENU-2" , 30 * "-")
    print ("1. Menu Option Secundaria 1")
    print ("2. Menu Option Secundaria 2")
    print ("3. Menu anterior")
    print (67 * "-")

opcion = True

while opcion:
    menu1()
    selec = int(input("Seleccione una opcion entre [1-4]: "))

    if selec == 1:
        opcion2 = True
        while opcion2:
            menu2()
            selec2 = int(input("Seleccione una opcion entre [1-3]: "))
            if selec2 == 1:
                print("1. Opcion secundaria 1")
            elif selec2 == 2:
                print("2. Opcion secundaria 2")
            elif selec2 == 3:
                opcion2 = False
            else:
                input("Selección incorrecta. Introduce cualquier tecla para volver a intentarlo...")
    elif selec == 2:
        opcion2 = True
        while opcion2:
            menu2()
            selec2 = int(input("Seleccione una opcion entre [1-3]: "))
            if selec2 == 1:
                print("1. Opcion secundaria 1")
            elif selec2 == 2:
                print("2. Opcion secundaria 2")
            elif selec2 == 3:
                opcion2 = False
            else:
                input("Selección incorrecta. Introduce cualquier tecla para volver a intentarlo...")
    elif selec == 3:
        opcion2 = True
        while opcion2:
            menu2()
            selec2 = int(input("Seleccione una opcion entre [1-3]: "))
            if selec2 == 1:
                print("1. Opcion secundaria 1")
            elif selec2 == 2:
                print("2. Opcion secundaria 2")
            elif selec2 == 3:
                opcion2 = False
            else:
                input("Selección incorrecta. Introduce cualquier tecla para volver a intentarlo...")
    elif selec == 4:
        opcion = False
    else:
        input("Selección incorrecta. Introduce cualquier tecla para volver a intentarlo...")'''

'''# 3)Desarrolle un ejercicio programado, donde ejemplifique el uso de la herencia, el ejemplo
#no puede ser los mismos vistos en clase, debe ser de autoría del estudiante. Este ejemplo
#debe tener las clases, la instancia y el consumo de los atributos y métodos de la clase de
#la cual hereda.

class Cosa:
    def __init__(self, cantidad = 100):
        self.cantidad = cantidad
    def algo(self, msj):
        print(msj)

class Casa(Cosa):
    def caracteristica(self, feo):
        print("Algo:", feo)

class Tipo(Cosa):
    def cualidad(self, aspero):
        print("Tla vez es:", aspero)

casas = Casa()
tipos =Tipo(99)

casas.caracteristica("Raro")
tipos.cualidad("Aspero")
print("La cantidad de casas es ",casas.cantidad)
print("La cantidad de tipos es ",tipos.cantidad)'''


# 4)El siguiente ejemplo de código contiene errores corríjalos para que el ejemplo se ejecute
#de manera correcta, indique en comentarios cuales fueron los errores encontrados.

class Articulo:
    def __init__(self,nombre,codigo):
        self.nombre = nombre
        self.codigo = codigo
    def getNombre(self):
        return (self.nombre)
    def getCodigo(self):
        return (self.codigo)

class Juguete(Articulo):
    def __init__(self,nombre,precio,codigo):
        super().__init__(nombre,codigo)
        self.__precio = precio

    def getDescripcion(self):
        return self.getNombre() + self.__precio + " codigo " + self.getCodigo()

objeto = Juguete("Carro"," precio 1234 ","147")

print(objeto.getDescripcion())
print(objeto.getNombre())

#Se agrego la clase Articulo y los metodos getNombre, getCodigo y se llamo el objeto al final.
